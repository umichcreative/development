# Drupal Development

## Development workflow
Whenever possible it is recommended to use Composer to manage Drupal core and contributed modules.

## Local development
Lando should be used for local Drupal development. A .lando.yml file should be present the project directory. To start the development environment, run:

```
lando start
```

Lando should output some URLs that can be used to access the site.

## New project
New Drupal projects should be created from the following repo (replace site-name instances with your project name):

```
git clone https://gitlab.com/umichcreative/drupal-starter-kit site-name && rm -rf site-name/.git && cd site-name
```

This will download the necessary files into the site-name directory and remove the .git folder since we'll want to start our own repo.

Update the name field in .lando.yml, this will prevent any collisions with other projects.

A new repo will need to be created in our Gitlab account:
https://gitlab.com/umichcreative

Once you have created init a new git repo in your new project directory:

```
git init
```

Add your new code to the repo:

```
git add .
```

Make your first commit:

```
git commit -m "Initial commit"
```

Follow the instructions from GitLab to add a remote to your repo:

```
git remote add origin repo-path-from-gitlab
```

Push it!

```
git push origin master
```

Spin up the local development environment with:

```
lando start
```

This will start the web and database containers and download all depencdencies using Composer.

We can install Drupal locally with a Drush command:

```
lando drush si minimal --db-url=mysql://drupal8:drupal8@database/drupal8 -y
```

The output should contain an admin login and password.

Use the Drupal Console to set the site to development mode:

```
chmod 755 web/sites/default
lando drupal site:mode dev
```

You can now visit the URL that Lando referenced earlier to begin working.