#!/usr/bin/env bash

# Pulls code from provided branch name.
git pull origin $1

# Install Composer dependencies
composer install

# Rebuild the Drupal cache.
./vendor/bin/drush cr

# Import any configuration changes.
./vendor/bin/drush cim -y

# Run and database updates.
./vendor/bin/drush updatedb -y

# Run any entity updates.
./vendor/bin/drush entup -y
