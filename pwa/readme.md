# PWA Resources

The files included in this directory are starter files for creating a PWA.

## Manifest

manifest.json should be included in the web root of your application. You'll want to load it in the head of your HTML file(s):

```
<link rel="manifest" href="/manifest.json">
```

The values should be updated to reflect your project. The icons included should be placed next to this file so they are properly loaded.

## Offline

A service worker file is included to give basic offline capabilities. It simply serves the offline.html file if a connection cannot be made.
The code inside the app.js file should be added to your project's primary JS file. This will load the sw.js file if the browser supports Service Workers.
The sw.js file should be placed in the web root of the project. The offline.html should also be added to the web root.

The offline.html contains inline styles and inlive svgs so only one file has to be cached for the user to see the offline experience.
