# Michigan Creative Development

This repository contains documentation and files for development.
Directories exist for common types of frameworks and tools we use.

## Recommended software
* [docker](https://www.docker.com/) - required for lando
* [lando](https://docs.lando.dev/) - backend development
* [nodejs](https://nodejs.org/en/) - frontend development
* [umichcreative-cli](https://gitlab.com/umichcreative/umichcreative-cli) - spinning up new projects
* [vscode](https://code.visualstudio.com/) - javascript / frontend development
* [phpstorm](https://www.jetbrains.com/phpstorm/) - php / backend development
