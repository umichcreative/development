# ITS Hosting

## File permissions
### Read access
fs sa directory umweb:servers read
### Write access
fs sa directory umweb:servers write
### Grant afs access to a group
pts adduser mktgcomm:web groupname
### Grant an individual user access to a group
pts adduser uniqname groupname
### Give an individual access rights to a directory
fs setacl folder uniqname write/all/read/none

## Other ITS server commands
### Check diskspace quote
fs lq

## Community Group
mcreativeweb

## Request Storage (AFS IFS)
http://its.umich.edu/computing/backup-storage/afs

## Request vhost
http://its.umich.edu/computing/web-mobile/web-application-hosting/virtual-hosting

## Request container
https://its.umich.edu/computing/virtualization-cloud/container-service/pricing

## Request database
http://its.umich.edu/enterprise/data-database/midatabase/request-midatabase

### MySQL docs
http://documentation.its.umich.edu/node/380

## Pending requests
https://services.it.umich.edu/itservicesportal/myrequests

## Common Commands that you won't run too often, but when you really need to use it you will have forgotten the exact syntax

`tar --exclude='html/wp-content/uploads' --exclude='html/wp-content/plugins/backwpup' --exclude='html/_oldsite' --exclude='html/_admin' -zcvf mla-staff.tar.gz html`

This is run from the directory up from /html
